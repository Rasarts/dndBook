library dnd.book;

import 'dart:html';
import 'dart:async';
import 'dart:indexed_db';
import 'package:idb_shim/idb_browser.dart';

part 'pages_events.dart';
part 'behavior.dart';
part 'character.dart';
part 'inventory.dart';
part 'skills.dart';

main() async {
  pagesListToggle();
  await makeBase();
  await makeCharacterPage();
  await makeInventoryPage();
  await makeSkillsPage();
}
