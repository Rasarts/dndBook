part of dnd.book;

makeSkillsPage() async {
  Element skillsSection = querySelector('.skills');

  skillsSection.appendHtml('Умения персонажа<br><br>');
  await createAddInput(forSection: skillsSection);

  skillsSection.appendHtml('<hr><br>');

  var store = await getStore(dataSkills);
  var cursorRequest = store.openCursor();
  cursorRequest.listen((event) async {
    var key = await event.key;
    var value = await store.getObject(key);
    new CreateInput.fromMap(forSection: skillsSection, key: key);
    event.next();
  });
}
