part of dnd.book;

makeCharacterPage() async {
  Element characterSection = querySelector('.character');
  createInputElement(
      forSection: characterSection,
      inputName: 'character-name',
      labelName: 'Имя персонажа');
  createInputElement(
      forSection: characterSection,
      inputName: 'character-hp',
      labelName: 'Хит-поинты');
  createInputElement(
      forSection: characterSection,
      inputName: 'character-class-lvl',
      labelName: 'Класс / уровень');
  createInputElement(
      forSection: characterSection,
      inputName: 'character-xp',
      labelName: 'Опыт');
  createInputElement(
      forSection: characterSection,
      inputName: 'character-ideology-religion',
      labelName: 'Мир / религия');
  createInputElement(
      forSection: characterSection,
      inputName: 'character-growth-weight',
      labelName: 'Рост / вес');

  characterSection.appendHtml('<hr>Показатели параметров<br><br>');

  createInputElement(
      forSection: characterSection,
      inputName: 'character-force',
      labelName: 'СИЛА');
  createInputElement(
      forSection: characterSection,
      inputName: 'character-agility',
      labelName: 'ЛОВКОСТЬ');
  createInputElement(
      forSection: characterSection,
      inputName: 'character-body',
      labelName: 'ТЕЛО');
  createInputElement(
      forSection: characterSection,
      inputName: 'character-intelligence',
      labelName: 'ИНТЕЛЕКТ');
  createInputElement(
      forSection: characterSection,
      inputName: 'character-wisdom',
      labelName: 'МУДРОСТЬ');
  createInputElement(
      forSection: characterSection,
      inputName: 'character-charm',
      labelName: 'ОБАЯНИЕ');

  characterSection.appendHtml('<hr>Выбор для сражений<br><br>');

  createInputElement(
      forSection: characterSection,
      inputName: 'character-basic-attack',
      labelName: 'Бонус атаки');
  createInputElement(
      forSection: characterSection,
      inputName: 'character-basic-weapon',
      labelName: 'Оружие');
  // labelName: 'Оружие / диапазон / повреждения / крит');
  createInputElement(
      forSection: characterSection,
      inputName: 'character-basic-weapon-damage-range-crit',
      labelName: 'Пов/диап/крит');

  characterSection.appendHtml('<hr>');

  createInputElement(
      forSection: characterSection,
      inputName: 'character-speed',
      labelName: 'Скорость');
  createInputElement(
      forSection: characterSection,
      inputName: 'character-initiative',
      labelName: 'Мод.инициатив');
  createInputElement(
      forSection: characterSection,
      inputName: 'character-seizure',
      labelName: 'Мод.захвата');

  characterSection.appendHtml('<hr>Спас-броски<br><br>');

  createInputElement(
      forSection: characterSection,
      inputName: 'character-persistence',
      labelName: 'Стойкость');
  createInputElement(
      forSection: characterSection,
      inputName: 'character-speed',
      labelName: 'Рефлекс');
  createInputElement(
      forSection: characterSection,
      inputName: 'character-will',
      labelName: 'Воля');

  characterSection.appendHtml('<hr>Класс доспеха<br><br>');

  createInputElement(
      forSection: characterSection,
      inputName: 'character-class-armor',
      labelName: 'КД');
}
