part of dnd.book;

makeInventoryPage() async {
  Element inventorySection = querySelector('.inventory');
  createInputElement(
      forSection: inventorySection,
      inputName: 'inventory-gold',
      labelName: 'Кол.денег');

  await createAddInput(forSection: inventorySection);

  inventorySection.appendHtml('<hr><br>');

  var store = await getStore(dataInventory);
  var cursorRequest = store.openCursor();
  cursorRequest.listen((event) async {
    var key = await event.key;
    var value = await store.getObject(key);
    new CreateInput.fromMap(forSection: inventorySection, key: key);
    event.next();
  });
}
