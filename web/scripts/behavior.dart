part of dnd.book;

IdbFactory idbFactory = getIdbFactory();
Database database;
const String dataBase = "base";
const String dataStore = "store";
const String dataCharacter = "character";
const String dataInventory = "inventory";
const String dataSkills = "skills";

makeBase() async {
  _onUpdate(VersionChangeEvent event) {
    Database db = event.database;
    db.createObjectStore(dataStore, autoIncrement: true);
    db.createObjectStore(dataCharacter, autoIncrement: true);
    db.createObjectStore(dataInventory, autoIncrement: true);
    db.createObjectStore(dataSkills, autoIncrement: true);
  }

  database =
      await idbFactory.open(dataBase, version: 4, onUpgradeNeeded: _onUpdate);
}

getStore(storeName) async {
  var transaction;
  transaction = await database.transaction(storeName, "readwrite");
  return await transaction.objectStore(storeName);
}

addToStore(storeName, value, key) async {
  var store = await getStore(storeName);
  store.put(value, key);
}

getFromStore(storeName, key) async {
  var store = await getStore(storeName);
  return store.getObject(key);
}

checkData(forSection) {
  var data;
  if (forSection.classes.contains('character')) {
    data = dataCharacter;
  } else if (forSection.classes.contains('inventory')) {
    data = dataInventory;
  } else if (forSection.classes.contains('skills')) {
    data = dataSkills;
  }
  return data;
}

class CreateInput {
  createLabel(data, String labelText, String key) {
    LabelElement label = new LabelElement();
    label.setAttribute('for', key);
    label.appendText('$labelText:');

    label.onClick.listen((event) async {
      var store = await getStore(data);
      store.delete(key);
      var inputFor = event.target.nextNode;
      inputFor.remove();
      event.target.remove();
    });

    return label;
  }

  createInputElementFromMap(data, String value, String key) {
    InputElement input = new InputElement();
    var attibutes = {'name': key, 'class': key, 'type': 'text'};
    input.attributes.addAll(attibutes);
    input.placeholder = value;

    var inStore;

    getFromStore(data, key).then((result) {
      inStore = result['value'];
    });

    input.onKeyUp.listen((event) async {
      var mapValues = await getFromStore(data, key);
      mapValues['value'] = event.target.value;
      addToStore(data, mapValues, key);
      input.placeholder = mapValues['value'];
    });

    input.onFocus.listen((event) {
      getFromStore(data, key).then((result) {
        event.target.value = result['value'];
      });
    });

    return input;
  }

  CreateInput.fromMap({Element forSection, String key}) {
    var data = checkData(forSection);
    Map valueOnTable;
    var label, value;

    getFromStore(data, key).then((result) {
      valueOnTable = result;
      if (result['label'] != '') {
        label = result['label'];
      } else {
        label = '';
      }
      if (result['value'] != '') {
        value = valueOnTable['value'];
      } else {
        value = '';
      }

      LabelElement labelForInput = createLabel(data, label, key);
      InputElement input = createInputElementFromMap(data, value, key);

      forSection.append(labelForInput);
      forSection.append(input);
    });
  }
}

// Create input for store
createInputElement({Element forSection, String inputName, String labelName}) {
  var data;
  if (forSection.classes.contains('character')) {
    data = dataCharacter;
  } else if (forSection.classes.contains('inventory')) {
    data = dataInventory;
  } else if (forSection.classes.contains('skills')) {
    data = dataSkills;
  }

  LabelElement label = new LabelElement();
  label.setAttribute('for', inputName);
  label.appendText('$labelName:');

  InputElement input = new InputElement();
  var attibutes = {'name': inputName, 'class': inputName, 'type': 'text'};
  input.attributes.addAll(attibutes);

  getFromStore(data, inputName).then((result) {
    input.placeholder = result.toString();
  });

  input.onSubmit.listen((event) {
    event.preventDefault();
  });

  input.onFocus.listen((event) {
    getFromStore(data, inputName).then((result) {
      event.target.value = result;
    });
  });

  input.onKeyUp.listen((event) {
    addToStore(data, event.target.value, inputName);
    input.placeholder = getFromStore(data, inputName).toString();
  });

  forSection.append(label);
  forSection.append(input);
}

// Add button
createAddInput({Element forSection}) async {
  var addBtn = new DivElement();
  addBtn.appendText('Добавить');
  addBtn.classes.add('addBtn');

  addBtn.onClick.listen((event) {
    var inputAddBaseId = new InputElement();
    inputAddBaseId.setAttribute('placeholder', 'baseId');
    forSection.append(inputAddBaseId);

    var inputAddlabel = new InputElement();
    inputAddlabel.setAttribute('placeholder', 'Название');
    forSection.append(inputAddlabel);

    var apply = new SpanElement();
    apply.appendText(' Создать');

    apply.onClick.listen((event) {
      Map values = new Map();
      values['label'] = inputAddlabel.value;
      values['value'] = 'Показатели';
      var key = inputAddBaseId.value;

      var data = checkData(forSection);

      if (inputAddlabel.value != '') {
        addToStore(data, values, key);
        new CreateInput.fromMap(forSection: forSection, key: key);
      }

      apply.remove();
      inputAddBaseId.remove();
      inputAddlabel.remove();
    });

    forSection.append(apply);
  });

  forSection.append(addBtn);
}
