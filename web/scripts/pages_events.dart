part of dnd.book;

var marks, sections, index;

pagesListToggle() {
  marks = querySelectorAll('.menu-icon');
  sections = querySelectorAll('.list');

  sections.classes.add('closeList');
  sections[0].classes.remove('closeList');
  sections[0].classes.add('openList');

  _makeEventsHandler(element) {
    element.on['click'].listen((event) {
      var li = event.target;
      index = marks.indexOf(event.target);

      sections.classes.add('closeList');
      sections[index].classes.remove('closeList');
      sections[index].classes.add('openList');

      marks.classes.add('deactivate');
      li.classes.remove('deactivate');
      li.classes.add('activate');
    });
  }

  marks.forEach(_makeEventsHandler);
}
